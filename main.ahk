; This mostly works - it does what I need.

; Defaults
#NoEnv
#SingleInstance force
#Warn

SendMode Input
SetWorkingDir %A_ScriptDir%
AutoTrim, Off
SetTitleMatchMode RegEx
SetNumlockState, AlwaysOn

EnvGet, homedir, USERPROFILE

; Shortcuts
;; Swap CapsLock/Ecs for VIM
;; A better way to remap single keys is in the registry, using sharpkeys.
;;CapsLock::Esc
;;Esc::CapsLock

;; Remap Alt+Tab
Ctrl & PgUp::AltTab
Ctrl & PgDn::ShiftAltTab

;; Volume Up/Down
^!Up::SoundSet,+5
^!Down::SoundSet,-5

; Jump to App
LaunchOrSwitchWithExe(AhkExe, ExeName)
{
    IfWinNotExist, ahk_exe %AhkExe%
        Run, %ExeName%
    WinActivate, ahk_exe %AhkExe%
    Return
}

LaunchOrSwitchWithClass(AhkClass, ExeName)
{
    IfWinNotExist, ahk_class %AhkClass%
        Run, %ExeName%
    WinActivate, ahk_class %AhkClass%
    Return
}

FocusApplication()
{
    DetectHiddenWindows, Off
    Process, Exist, slack.exe
    procPid = %ErrorLevel%

    IfWinNotActive, ahk_pid %procPid%
       WinActivate, ahk_pid %procPid%

    Return
}

LaunchSystray(Proc, Cmd)
{
    Process, Exist, %Proc%
    If (ErrorLevel = 0)
	{
	    Run, %Cmd%
	}
    Return
}

FocusWindow(AhkClass)
{
    IfWinExist, ahk_class %AhkClass%
        WinActivate, ahk_class %AhkClass%
    Return
}

; Jump prefix: Ctl+Alt
^!w::LaunchOrSwitchWithClass("MozillaWindowClass", "C:\Program Files\Mozilla Firefox\firefox.exe")
;^!c::LaunchOrSwitchWithExe("Code.exe",  %USERPROFILE%\AppData\Local\Programs\Microsoft VS Code\Code.exe)
^!o::LaunchOrSwitchWithClass("rctrl_renwnd32", "C:\Program Files\Microsoft Office\root\Office16\OUTLOOK.EXE")
^!s::FocusApplication()
^!t::LaunchOrSwitchWithClass("CASCADIA_HOSTING_WINDOW_CLASS", "wt.exe")
^!n::LaunchOrSwitchWithClass("Notepad", "notepad.exe")
;; focus zoom meeting
^!z::FocusWindow("ZPContentViewWndClass")
