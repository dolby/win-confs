" This file is for VIM in Git-Bash

" load the defaults
source $VIMRUNTIME/defaults.vim

call plug#begin('~/.config/nvim/plugged')
Plug 'https://gitlab.com/protesilaos/tempus-themes-vim.git'
Plug 'airblade/vim-gitgutter'
Plug 'itchyny/lightline.vim'
Plug 'ntpeters/vim-better-whitespace'
Plug 'tpope/vim-surround'
Plug 'jiangmiao/auto-pairs'
Plug 'justinmk/vim-dirvish'
call plug#end()

" theme
if has('termguicolors')
  set termguicolors
endif
syntax on
try
  colorscheme tempus_summer
catch
endtry

set autoindent
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set smarttab

set autowrite
set clipboard+=unnamed
set nowrap
set number relativenumber
set pastetoggle=<F5>
set showmatch
set smartcase

" splits/edits
set splitbelow
set splitright
noremap <leader>s :vsplit<CR>
noremap <leader>h :split<CR>
noremap <leader>o :e .<CR>
noremap <silent> <C-Up> :res +2<CR>
noremap <silent> <C-Down> :res -2<CR>
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>

" filetypes
au BufNewFile,BufRead *.json setlocal expandtab ts=2 sw=2
au BufNewFile,BufRead *.md setlocal spell noet ts=4 sw=4
au BufNewFile,BufRead *.txt setlocal noet ts=4 sw=4
au BufNewFile,BufRead *.yml,*.yaml setlocal expandtab ts=2 sw=2

" plugins
"
"" better whitespace
let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1
autocmd FileType markdown EnableWhitespace
"" lightline
let g:lightline = { 'colorscheme': 'powerlineish' }
set noshowmode
set laststatus=2

